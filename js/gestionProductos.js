/*variable global para poder usarla en ambas funciones*/
var productosObtenidos;

function getProductos(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

/*configuracion de la peticion*/
  request.onreadystatechange = function(){


    //200 respondio bien, status 4= cargado, continuar
    if (this.readyState == 4 && this.status == 200) {
      //pintar el resultado en la console f12
        console.log(request.responseText);
        //objeto JSON obtenido request.responseText
        productosObtenidos = request.responseText;
        procesarProductos();

    }
  }

  request.open("GET",url,true);
  request.send();

}

function procesarProductos(){
  // ALMACENA LA LISTA DE PRODCUTOS
    var JSONProductos = JSON.parse(productosObtenidos);
    //se trae el objeto del HTML
    var divTabla = document.getElementById("divTabla");
    //se crea un elmento HTML tipo table
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    //pinta la tabla en cebra
    tabla.classList.add("table-striped");

  //  alert(JSONProductos.value[0].ProductName);
    for (var i = 0; i < JSONProductos.value.length; i++) {
  //    console.log(JSONProductos.value[i].ProductName);
      //  filas
      var nuevaFila = document.createElement("tr");
      // columna Nombre
      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = JSONProductos.value[i].ProductName;
      // columna Precio
      var columnaPrecio = document.createElement("td");
      columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
      // columna Stock
      var columnaStock = document.createElement("td");
      columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

      //agregar los 3 elemntos  a las filas
      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaPrecio);
      nuevaFila.appendChild(columnaStock);

      tbody.appendChild(nuevaFila);

    }
    //agregar la files al tabla
    tabla.appendChild(tbody);
    //agregar la tabla al divTabla
    divTabla.appendChild(tabla);
}
