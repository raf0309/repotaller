var clientesObtenidos;

function getClientes(){

  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
/*configuracion de la peticion*/
  request.onreadystatechange = function(){
    /*200 respondio bien, status 4= cargado, continuar*/
    if (this.readyState == 4 && this.status == 200) {
      /*pintar el resultado en la console f12*/
        console.log(request.responseText);
        clientesObtenidos = request.responseText;
        procesarClientes();

    }
  }

  request.open("GET",url,true);
  request.send();

}


function procesarClientes(){
  // ALMACENA LA LISTA DE PRODCUTOS
    var JSONClientes = JSON.parse(clientesObtenidos);
    //se trae el objeto del HTML
    var divTabla = document.getElementById("divTablaC");
    //se crea un elmento HTML tipo table
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    //pinta la tabla en cebra
    tabla.classList.add("table-striped");

  //  alert(JSONProductos.value[0].ProductName);
    for (var i = 0; i < JSONClientes.value.length; i++) {
  //    console.log(JSONProductos.value[i].ProductName);
      //  filas
      var nuevaFila = document.createElement("tr");
      // columna ID
      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = JSONClientes.value[i].CompanyName;
      // columna Precio
      var columnaCiudad = document.createElement("td");
      columnaCiudad.innerText = JSONClientes.value[i].City;
      // columna BANDERA
      var columnaBandera = document.createElement("td");
      columnaBandera.innerText = JSONClientes.value[i].Country;

      var urlBandera     ="https://www.countries-ofthe-world.com/flags-normal/flag-of-";
      var imgBandera =document.createElement("img");
      //agregar el estilo del css
      imgBandera.classList.add("flag");

      if (JSONClientes.value[i].Country == "UK")
      {
        imgBandera.src =urlBandera.concat("United-Kingdom").concat(".png");
      }
      else{
          imgBandera.src =urlBandera.concat(JSONClientes.value[i].Country).concat(".png");
      }

      //agregar los 3 elemntos  a las filas
      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaCiudad);
      nuevaFila.appendChild(columnaBandera);
      nuevaFila.appendChild(imgBandera);

      tbody.appendChild(nuevaFila);

    }
    //agregar la files al tabla
    tabla.appendChild(tbody);
    //agregar la tabla al divTabla
    divTablaC.appendChild(tabla);
}
